﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GroupLab.iNetwork;
using GroupLab.iNetwork.Tcp;

using Phidgets;
using Phidgets.Events;
using System.Diagnostics;
using System.Windows.Threading;

namespace StatusUpdaterPhidget
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Class Instance Variables
        private Server _server;
        private List<Connection> _clients;
        #endregion

        Servo servo = new Servo();
        InterfaceKit ik = new InterfaceKit();
        TextLCD lcd = new TextLCD();
        LED led = new LED();
        DispatcherTimer wheelTimer = new DispatcherTimer();
        DispatcherTimer walkTimer = new DispatcherTimer();
        DispatcherTimer pencilTimer = new DispatcherTimer();
        DispatcherTimer stopTimer = new DispatcherTimer();
        DispatcherTimer lcdTimer = new DispatcherTimer();
        bool direction = true;
        string lastStatus = "";
        int alternateLCD = 0;

        #region Constructors
        public MainWindow()
        {
            InitializeComponent();
            InitializeServer();
            wheelTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000);
            wheelTimer.Tick += new EventHandler(wheelTimer_Tick);
            walkTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            walkTimer.Tick += new EventHandler(walkTimer_Tick);
            pencilTimer.Interval = new TimeSpan(0, 0, 0, 0, 300);
            pencilTimer.Tick += new EventHandler(pencilTimer_Tick);
            stopTimer.Interval = new TimeSpan(0, 0, 0, 4);
            stopTimer.Tick += new EventHandler(stopTimer_Tick);
            lcdTimer.Interval = new TimeSpan(0, 0, 0, 0, 400);
            lcdTimer.Tick += lcdTimer_Tick;

            this.Closing += new CancelEventHandler(OnWindowClosing);

            // Invoked when the interface kit is actually plugged in (Attached)
            ik.Attach += ik_Attach;
            ik.open();
            // Invoked when the servo is actually plugged in (Attached)
            servo.Attach += servo_Attach;
            servo.open();

            lcd.open();
            led.open();
            
        }

        void lcdTimer_Tick(object sender, EventArgs e)
        {
            //lcd.Attach += lcd_Attach;
            if (alternateLCD % 4 == 0)
            {
                lcd.rows[0].DisplayString = "Now Playing";               
            }
            else if (alternateLCD % 4 == 1)
            {
                lcd.rows[0].DisplayString = "Now Playing.";
            }
            else if (alternateLCD % 4 == 2)
            {
                lcd.rows[0].DisplayString = "Now Playing..";
            }
            else if (alternateLCD % 4 == 3)
            {
                lcd.rows[0].DisplayString = "Now Playing...";
            }
            alternateLCD++;
        }


        private void stopTimer_Tick(object sender, EventArgs e)
        {
            wheelTimer.Stop();
            pencilTimer.Stop();
            walkTimer.Stop();
            stopTimer.Stop();
            if (lastStatus != "Sleeping")
            {
                this.servo.servos[0].Position = 110;
            }
        }

        private void pencilTimer_Tick(object sender, EventArgs e)
        {
            if (direction == true)
            {
                this.servo.servos[2].Position = 0;
                direction = false;
            }
            else
            {
                this.servo.servos[2].Position = 180;
                direction = true;
            }
        }

        private void walkTimer_Tick(object sender, EventArgs e)
        {
            if (direction == true)
            {
                this.servo.servos[0].Position = 80;
                direction = false;
            }
            else
            {
                this.servo.servos[0].Position = 130;
                direction = true;
            }
        }

        void wheelTimer_Tick(object sender, EventArgs e)
        {
            if (direction == true)
            {
                this.servo.servos[3].Position = 0;
                direction = false;
            }
            else
            {
                this.servo.servos[3].Position = 180;
                direction = true;
            }
        }
        #endregion


        #region Initialization
        private void InitializeServer()
        {
            this._clients = new List<Connection>();

            // Creates a new server with the given name and is bound to the given port.
            this._server = new Server("Name-of-Service", 12345);
            this._server.IsDiscoverable = true;
            this._server.Connection += new ConnectionEventHandler(OnServerConnection);
            this._server.Start();

            this._statusLabel.Content = "IP: " + this._server.Configuration.IPAddress.ToString()
                + ", Port: " + this._server.Configuration.Port.ToString();
        }

        // Handles the event that a connection is made
        private void OnServerConnection(object sender, ConnectionEventArgs e)
        {
            if (e.ConnectionEvent == ConnectionEvents.Connect)
            {
                // Lock the list so that only the networking thread affects it
                lock (this._clients)
                {
                    if (!(this._clients.Contains(e.Connection)))
                    {
                        // Add to list and create event listener
                        this._clients.Add(e.Connection);
                        e.Connection.MessageReceived += new ConnectionMessageEventHandler(OnConnectionMessage);

                        // Using the GUI thread make changes
                        this.Dispatcher.Invoke(
                            new Action(
                                delegate()
                                {
                                    this._clientsList.Items.Add(e.Connection);
                                }
                        ));
                    }
                }
            }
            else if (e.ConnectionEvent == ConnectionEvents.Disconnect)
            {
                // Lock the list so that only the networking thread affects it
                lock (this._clients)
                {
                    if (this._clients.Contains(e.Connection))
                    {
                        // Clean up --  remove from list and remove event listener
                        this._clients.Remove(e.Connection);
                        e.Connection.MessageReceived -= new ConnectionMessageEventHandler(OnConnectionMessage);

                        // Using the GUI thread make changes
                        this.Dispatcher.Invoke(
                            new Action(
                                delegate()
                                {
                                    this._clientsList.Items.Remove(e.Connection);
                                }
                        ));
                    }
                }
            }
        }
        #endregion


        #region Main Body
        // Handles the event that a message is received
        private void OnConnectionMessage(object sender, Message msg)
        {
            if (msg != null)
            {
                
                // Check message name...
                switch (msg.Name)
                {
                    default:
                        //Do nothing
                        break;



                    // Servo 0 = Figure's vertical rotation
                    // Servo 1 = Figure's horizontal rotation
                    // Servo 2 = Pencil rotation
                    // Servo 3 = Wheel rotation
                    case "Sleeping":
                        
                        this.Dispatcher.Invoke(new Action(delegate()
                        {
                            
                            //if servo is attached, then get servo number zero and set its position based on the value of the sensor
                            if (this.servo.Attached)
                            {
                                Debug.WriteLine("Sleeping");
                                this.servo.servos[1].Position = 90;
                                this.servo.servos[0].Position = 180;
                                //this.servo.servos[3].Position = 0;
                                wheelTimer.Stop();
                                walkTimer.Stop();
                                pencilTimer.Stop();
                                lastStatus = "Sleeping";
                                stopTimer.Start();
                                lcdTimer.Stop();
                                lcd.rows[0].DisplayString = "Closed";
                                lcd.Backlight = false;
                                for (int i = 0; i < ik.outputs.Count; i++)
                                {
                                    ik.outputs[0] = false;
                                }
                            }
                        }));
                        break;
                    case "Driving":
                        
                        this.Dispatcher.Invoke(new Action(delegate()
                        {
                            //if servo is attached, then get servo number zero and set its position based on the value of the sensor
                            if (this.servo.Attached)
                            {
                                Debug.WriteLine("Driving");
                                this.servo.servos[1].Position = 0;
                                this.servo.servos[0].Position = 110;
                                
                                this.servo.servos[3].Position = 180;
                                wheelTimer.Start();
                                walkTimer.Stop();
                                pencilTimer.Stop();
                                lastStatus = "Driving";
                                stopTimer.Start();
                                lcdTimer.Stop();
                                lcd.rows[0].DisplayString = "Closed";
                                lcd.Backlight = false;
                                for (int i = 0; i < ik.outputs.Count; i++)
                                {
                                    ik.outputs[0] = false;
                                }
                            }
                        }));
                        break;
                    case "InClass":
                        
                        this.Dispatcher.Invoke(new Action(delegate()
                        {
                            //if servo is attached, then get servo number zero and set its position based on the value of the sensor
                            if (this.servo.Attached)
                            {
                                Debug.WriteLine("InClass");
                                this.servo.servos[1].Position = 60;
                                this.servo.servos[0].Position = 110;
                                //this.servo.servos[3].Position = 0;
                                wheelTimer.Stop();
                                walkTimer.Stop();
                                pencilTimer.Start();
                                lastStatus = "InClass";
                                stopTimer.Start();
                                lcdTimer.Stop();
                                lcd.rows[0].DisplayString = "Closed";
                                lcd.Backlight = false;
                                for (int i = 0; i < ik.outputs.Count; i++)
                                {
                                    ik.outputs[0] = false;
                                }
                            }
                        }));
                        break;
                    case "Home":
                        
                        this.Dispatcher.Invoke(new Action(delegate()
                        {
                            //if servo is attached, then get servo number zero and set its position based on the value of the sensor
                            if (this.servo.Attached)
                            {
                                Debug.WriteLine("Home");
                                this.servo.servos[1].Position = 180;
                                this.servo.servos[0].Position = 110;
                                //this.servo.servos[3].Position = 0;
                                wheelTimer.Stop();
                                walkTimer.Stop();
                                pencilTimer.Stop();
                                lastStatus = "Home";
                                stopTimer.Start();
                                lcdTimer.Stop();
                                lcd.rows[0].DisplayString = "Closed";
                                lcd.Backlight = false;
                                for (int i = 0; i < ik.outputs.Count; i++)
                                {
                                    ik.outputs[0] = true;
                                }
                            }
                        }));
                        break;
                    case "Movie":
                        
                        this.Dispatcher.Invoke(new Action(delegate()
                        {
                            //if servo is attached, then get servo number zero and set its position based on the value of the sensor
                            if (this.servo.Attached)
                            {
                                Debug.WriteLine("Movie");
                                this.servo.servos[1].Position = 120;
                                this.servo.servos[0].Position = 110;
                                //this.servo.servos[3].Position = 0;
                                wheelTimer.Stop();
                                walkTimer.Stop();
                                pencilTimer.Stop();
                                lastStatus = "Movie";
                                lcdTimer.Start();
                                lcd.Backlight = true;
                                stopTimer.Start();
                                for (int i = 0; i < ik.outputs.Count; i++)
                                {
                                    ik.outputs[0] = false;
                                }
                            }
                        }));
                        break;
                    case "Walking":
                        
                        this.Dispatcher.Invoke(new Action(delegate()
                        {
                            //if servo is attached, then get servo number zero and set its position based on the value of the sensor
                            if (this.servo.Attached)
                            {
                                Debug.WriteLine("Walking");
                                
                                //this.servo.servos[3].Position = 0;
                                wheelTimer.Stop();
                                walkTimer.Start();
                                pencilTimer.Stop();
                                lastStatus = "Walking";
                                stopTimer.Start();
                                lcdTimer.Stop();
                                lcd.rows[0].DisplayString = "Closed";
                                lcd.Backlight = false;
                                for (int i = 0; i < ik.outputs.Count; i++)
                                {
                                    ik.outputs[0] = false;
                                }
                            }
                        }));
                        break;
                        
                        
                }
            }
        }

        // When server window closes, stop running the server
        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this._server != null && this._server.IsRunning)
            {
                this._server.Stop();
            }
        }



        #endregion

        private void lcd_Attach(object sender, AttachEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void servo_Attach(object sender, AttachEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(delegate()
            {
                //set the title indicating that the servo is attached
                //this.Title = ("servo " + servo.SerialNumber + " is attached");

                //add Detach event handler (optionally you can add Error event handler)
                servo.Detach += servo_Detach; ;  // generic phidget callbacks
                servo.Error += servo_Error;
            }));
        }


        private void ik_Attach(object sender, AttachEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(delegate()
            {
                //set the title indicating that the InterfaceKit is attached
                this.Title = ("InterfaceKit " + ik.SerialNumber + " is attached");
                //add detach event handler
                ik.Detach += ik_Detach;
                ik.Error += ik_Error;

                ik.SensorChange += ik_SensorChange;
            }));
        }

        void ik_Detach(object sender, DetachEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(delegate()
            {
                //set the title to reflect that interfaceKit has detached
                this.Title = ("InterfaceKit " + ik.SerialNumber + " is detached");
                //remove detach event handler for ik
                ik.Detach -= ik_Detach;
                ik.Error -= ik_Error;
                ik.SensorChange -= ik_SensorChange;
            }));
        }

        void servo_Detach(object sender, DetachEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(delegate()
            {
                //set the title to reflect that servo has detached
                //this.Title = ("servo " + servo.SerialNumber + " is detaached");
                //remove detach event handler for servo
                servo.Detach -= servo_Detach; ;  // generitc phdiget callbacks
                servo.Error -= servo_Error;

            }));
        }

        private void servo_Error(object sender, ErrorEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(delegate()
            {
                this.Title = "servo Error: " + e.Description;
            }));
        }

        private void ik_Error(object sender, ErrorEventArgs e)
        {
            this.Dispatcher.Invoke(new Action(delegate()
            {
                this.Title = "InterfaceKit Error: " + e.Description;
            }));
        }

        // A phidget's sensor value (which ranges between 0-1000) has changed
        void ik_SensorChange(object sender, SensorChangeEventArgs e)
        {
            // Ignore all but the first sensor
            //...
            if (e.Index != 1) return;

            this.Dispatcher.Invoke(new Action(delegate()
            {
                
                if (this.ik.sensors[e.Index].Value < 100)
                {
                    Debug.WriteLine("TOUCHED");
                    if (lastStatus == "Driving")
                    {
                        wheelTimer.Start();                      
                    }
                    if (lastStatus == "InClass")
                    {
                        pencilTimer.Start();
                    }
                    if (lastStatus == "Walking")
                    {
                        walkTimer.Start();
                    }
                    if (lastStatus == "Movie")
                    {
                        lcdTimer.Start();
                        lcd.Backlight = true;
                    }
                    stopTimer.Start();
                }
                this.Title = "IK Sensor 0 event at " + e.Value;

            }));
        }
    }
}
