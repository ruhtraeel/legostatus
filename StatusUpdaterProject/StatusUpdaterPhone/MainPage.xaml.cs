﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using GroupLab.iNetwork;
using GroupLab.iNetwork.Tcp;

namespace StatusUpdaterPhone
{


    public partial class MainPage : PhoneApplicationPage
    {

        private Connection _connection;
        private string _ipAddress = "192.168.1.17";
        private int _port = 12345;
        // Constructor


        #region iNetwork Methods

        private void InitializeConnection()
        {
            // connect to the server
            this._connection = new Connection(this._ipAddress, this._port);
            this._connection.Connected += new ConnectionEventHandler(OnConnected);
            this._connection.Start();
        }

        void OnConnected(object sender, ConnectionEventArgs e)
        {
            this._connection.MessageReceived += new ConnectionMessageEventHandler(OnMessageReceived);
        }

        private void OnMessageReceived(object sender, Message msg)
        {
            try
            {
                if (msg != null)
                {
                    //switch (msg.Name)
                    //{
                    //    default:
                    //        // don't do anything
                    //        break;
                    //    case "ChatMessage":
                    //        this.Dispatcher.BeginInvoke(new Action(delegate()
                    //        {
                    //            string userName = msg.GetStringField("UserName");
                    //            string content = msg.GetStringField("Content");
                    //            string message = userName + ": " + content + "\n";

                    //            this.chatMessages.Text += message;
                    //        }));


                    //        break;
                    //}
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + "\n" + e.StackTrace);
            }

        }

        #endregion

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            InitializeConnection();
            currentStatus.Text = "Home";
            //sendButton.Click += sendButton_Click;

        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            ////create a new message with the name ChatMessage
            //Message msg = new Message("ChatMessage");

            ////add username field and value to the message
            //msg.AddField("UserName", this.userNameTextbox.Text);
            ////add content to the message
            //msg.AddField("Content", this.currentMessage.Text);

            ////send the message through the connection
            //this._connection.SendMessage(msg);

            ////clear currentMessage.Text for further messages
            //currentMessage.Text = "";
        }

        private void InClass_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.Text = "In Class";
            Message msg = new Message("InClass");
            this._connection.SendMessage(msg);
        }

        private void Driving_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.Text = "Driving";
            Message msg = new Message("Driving");
            this._connection.SendMessage(msg);
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.Text = "Home";
            Message msg = new Message("Home");
            this._connection.SendMessage(msg);
        }

        private void Sleeping_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.Text = "Sleeping";
            Message msg = new Message("Sleeping");
            this._connection.SendMessage(msg);
        }

        private void Movie_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.Text = "Movie";
            Message msg = new Message("Movie");
            this._connection.SendMessage(msg);
        }

        private void Walking_Click(object sender, RoutedEventArgs e)
        {
            currentStatus.Text = "Walking";
            Message msg = new Message("Walking");
            this._connection.SendMessage(msg);
        }
    }
}